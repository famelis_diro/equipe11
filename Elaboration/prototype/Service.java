
public class Service extends ID {
	//Atributtes
	protected String nom;
	protected short capacite;
	protected short prix;
	
	public Service (String nom, short prix) {
		super(7);
		this.nom = nom;
		this.prix = prix;
	}
	
	//Getters
	public short get_Capacite() {
		return this.capacite;
	}
	public short get_Prix() {
		return this.prix;
	}
	
	//Setters
	public void set_Prix(short prix) {
		this.prix = prix;
	}
	public void set_Capacite(short capacite) {
		this.capacite = capacite;
	}
	
	//Overriding toString method to write in file
	public String toString() {
		String result = "";
		result += nom + "\t" + id + "\t" + prix + "\n";
		return result;
	}
}
