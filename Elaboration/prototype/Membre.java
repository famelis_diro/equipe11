
public class Membre extends Compte {
	
	//Atributtes
	private boolean statut;
	
	public Membre() {
		super();
		this.statut = true;
	}
	
	//overriding method
	public String toString() {
		String result = super.toString();
		return result += this.statut + "\n";
		
	}

}
