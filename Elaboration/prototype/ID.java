
import java.util.Random;

public abstract class ID {
	
	//Attributes
	protected String id;
	
	public ID (int longueurID) {
		String result = "";
		Random rando = new Random();
		for(int i=1; i<=longueurID; i++) {
			result += rando.nextInt(10);
		}
		this.id = result;
	}
	
	//Getter
	public String get_ID() {
		return this.id;
	}
}
