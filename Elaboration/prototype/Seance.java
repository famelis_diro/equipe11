import java.util.Arrays;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;



public class Seance {
	
	//Attributes
	private Service service;
	private String currentDate;
	private String dateDebutSerive;
	private String dateFinService;
	private String heureService;
	private String professionnelID;
	private String[] reccurence;
	private boolean disponibilite;
	private ArrayList<Membre> inscrits;
	
	
	public Seance (Service service, String professionnel) {
		this.service = service;
		
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		this.currentDate = sdf.format(date);
		
		this.professionnelID = professionnel;
		this.disponibilite = true;
		this.inscrits = new ArrayList<Membre> (service.get_Capacite());
	}
	
	
	//Adding Membre (inscription)
	public boolean addMembre (Membre membre) {
		if(inscrits.size() < service.get_Capacite()) {
			inscrits.add(membre);
			return true;
		}
		else {
			return false;
		}
	}
	
	// Setters
	public void set_dateDebutService(String date) {
		this.dateDebutSerive = date;
	}
	public void set_dateFinService(String date) {
		this.dateFinService = date;
	}
	public void set_heureService (String heure) {
		this.heureService = heure;
	}
	public void set_Reccurence(String[] reccurence) {
		this.reccurence = reccurence;
	}
	
	//Getters
	public Service get_Service() {
		return this.service;
	}
	
	
	// Overriding toString method to write in file
	public String toString() {
		String result = "";
		result += "Date et heure actuelles : " + this.currentDate + "\n";
		result += "Recurrence hebdomadaire du service :" + Arrays.toString(this.reccurence) + "\n";
		result += "Capacite maximale : " + service.get_Capacite() + "\n";
		result += "Numero du professionnel : " + this.professionnelID + "\n";
		result += "Code du service : " + service.get_ID() + "\n";
		result += "Frais du service : " + service.get_Prix() + "\n\n";
		return result;
	}
}
