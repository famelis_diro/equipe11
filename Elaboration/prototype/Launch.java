import java.util.Scanner;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;

public class Launch {
	
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Bienvenue dans #GYM");
		int n = 10;
		
		while (n!=0) {
			
			System.out.println("Entrer une commande\n1 pour Adherer\n2 pour Verification\n3 pour Cr�er une seance\n0 pour Quitter");
			n = reader.nextInt();
			//Adherer
			if (n==1) {
				Compte new_Membre = adherer(reader);
				if(new_Membre == null) {
					continue;
				}
				if(new_Membre instanceof Membre) {
					writer("membres", new_Membre.toString());
				}
				else {
					writer("professionnels", new_Membre.toString());
				}
				
				System.out.println("Compte Cr�� !");
			}
			//Verification
			else if(n==2) {
				verification(reader);
			}
			//Creer une seance
			else if (n==3) {
				Seance nouvelleSeance = ajouterSeance(reader);
				writer("seance", nouvelleSeance.toString());
			}
		}
		System.out.println("Au revoir !");
		reader.close();

	}
	
	
	public static Compte adherer (Scanner reader) {
		String info = reader.nextLine();
		Compte nouveau;
		
		System.out.println("Adhesion pour Membre ou Professionnel");
		info = reader.nextLine();
		if(info.equals("Membre")) {
			nouveau = new Membre();
		}
		else if (info.equals("Professionnel")) {
			nouveau = new Professionnel();
		}
		else {
			System.out.println("Choix non valide ! R�essayez de nouveau !\n");
			return null;
		}
		
		System.out.println("Entrez le nom");
		info = reader.nextLine();
		nouveau.set_Nom(info);
		
		System.out.println("Entrez le prenom");
		info = reader.nextLine();
		nouveau.set_Prenom(info);
		
		System.out.println("Entrez le numero de telephone");
		info = reader.nextLine();
		nouveau.set_Telephone(info);
		
		System.out.println("Entrez l'adresse");
		info = reader.nextLine();
		nouveau.set_Adresse(info);
		
		
		return nouveau;
	}
	
	public static void verification (Scanner reader) {
		String line;
		
		String info = reader.nextLine();
		System.out.println("Entrez le membre ID");
		info = reader.nextLine();
		
		
		try(BufferedReader br = new BufferedReader(new FileReader(".\\membre.txt"));)
		{
			while((line = br.readLine()) != null) {
				if(line.equals(info)) {
					for(int i=0;i<4;i++) {
						line = br.readLine();
					}
					if(line.equals("true")) {
						System.out.println("Acc�s acc�pt� !\n");
						return;
					}
					else {
						System.out.println("Membre suspendu");
						break;
					}
				}
				
				else {
					for(int i=0;i<5;i++) {
						br.readLine();
					}
				}
			}
			System.out.println("Acc�s refus� !\n");
			br.close();
		} catch (IOException e) {
			System.err.println("IOException : "+e.getMessage());
		}
	}

	
	public static Seance ajouterSeance (Scanner reader) {
		String info = reader.nextLine();
		Seance seance = null;
		System.out.println("Entrer le nom du service\n(Dans ce prototype c'est toujours le tennis qui utilis� comme nom\n[voir l'implantation du code])");
		info = reader.nextLine();
		//TODO: verifier si le service existe et avoir le service ID sinon creer un nouveau service.
		
		System.out.println("Entrer le numero du professionnel");
		info = reader.nextLine();
		seance = new Seance (new Service("Tennis", (short)10), info);
		
		System.out.println("Entrer la date du debut de service (JJ-MM-AAAA)");
		info = reader.nextLine();
		seance.set_dateDebutService(info);
		
		System.out.println("Entrer la date de fin de service (JJ-MM-AAAA)");
		info = reader.nextLine();
		seance.set_dateFinService(info);
		
		System.out.println("Entrer l'heure du service service (HH:MM)");
		info = reader.nextLine();
		seance.set_heureService(info);
		
		System.out.println("Entrer la reccurence hebdomadaire (jour1 jour2 ... jour7 )");
		info = reader.nextLine();
		String[] reccurence = info.split(" "); //Converting String to String[]
		seance.set_Reccurence(reccurence);
		
		System.out.println("Entrez la capacite maximale de la seance");
		info = reader.nextLine();
		seance.get_Service().set_Capacite(Short.valueOf(info)); //Casting String to short
		
		System.out.println("Entrez le prix de la seance");
		info = reader.nextLine();
		seance.get_Service().set_Prix(Short.valueOf(info));
		
		//TODO: Demander un commentaire
		System.out.println("Seance cree !");
		return seance;
	}
	
	public static ID trouverIDservice(String nomService) {
		//TODO : to implement 
		return null;
	}
	
	public static boolean siInscrit(Seance seance, Membre membre) {
		//TODO : to impliment : check if membre est inscrit dans la seance
		return true;
	}
	
	public static void rafraichirSeance () {
		//TODO : A implemanter : enleve tous les seances deja pass�es dans l'onglet Seances disponibles 
			  //(voir l'image de l'interface Accueil)
	}
	
	public static void inscrirMembre(Seance seance, Membre membre) {
		//TODO : A implemanter  
	}
	
	public static boolean confirmationSeance (Seance seance, Membre membre) {
		//TODO: A implemanter : Confirmer sa presense � une seance
		return true;
	}
	
	public static File liseInscrits (Seance seance) {
		//TODO : A implementer : retourne un fichier contenant tous les membre inscrit � la seance 
		return null;
	}
	
	public static void procedureComptable () {
		//TODO : A implemanter : Cree les fichiers TEF et l'envoie au g�rant et Service Comptable
	}
	
	public static File rapportSynthese () {
		//TODO : A implemanter : Cree un ficher Rapport de Synthese
		return null;
	}
	
	private static void writer(String type, String contenu) { 
		// Methode pour ecrire dans un fichier, le premier parametre est le nom du fichier a creer
		// Le second parametre est le contenu a ecrire dans le fichier
		try (FileWriter fw = new FileWriter(type+".txt", true);
			 BufferedWriter writer = new BufferedWriter(fw);
			 PrintWriter out = new PrintWriter(fw))					
		{
			out.println(contenu);
			
		} catch(IOException e) {
			System.err.println("IOException : "+ e.getMessage());
		}
	}

}
