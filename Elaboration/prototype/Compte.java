
public class Compte extends ID {

	//Attributes
	protected String nom;
	protected String prenom;
	protected String telephone;
	protected String adresse;
	
	public Compte() {
		super(9);
	}
	
	//Setters
	void set_Nom(String nom) {
		this.nom = nom;
	}
	void set_Prenom(String prenom) {
		this.prenom = prenom;
	}
	void set_Telephone(String telephone) {
		this.telephone = telephone;
	}
	void set_Adresse(String adresse) {
		this.adresse = adresse;
	}
	
	
	//overriding method
	public String toString() {
		String to_return = ""+ this.id +"\n";
		to_return = to_return + "\t" + this.nom + " " + this.prenom + "\n";
		to_return = to_return + "\t" + this.telephone + "\n";
		to_return = to_return + "\t" + this.adresse + "\n";
		return to_return;
	}
	
}
